const season2010_11 = [
  {
    "position": 1,
    "club": "Barcelona",
    "country": "es"
  },
  {
    "position": 2,
    "club": "Manchester Utd",
    "country": "eng"
  },
  {
    "position": 4,
    "club": "Real Madrid",
    "country": "es"
  },
  {
    "position": 4,
    "club": "Schalke 04",
    "country": "de"
  },
  {
    "position": 8,
    "club": "Chelsea",
    "country": "eng"
  },
  {
    "position": 8,
    "club": "Inter",
    "country": "it"
  },
  {
    "position": 8,
    "club": "Shakhtar",
    "country": "ua"
  },
  {
    "position": 8,
    "club": "Tottenham",
    "country": "eng"
  },
  {
    "position": 16,
    "club": "Arsenal",
    "country": "eng"
  },
  {
    "position": 16,
    "club": "Bayern Munich",
    "country": "de"
  },
  {
    "position": 16,
    "club": "FC Copenhagen",
    "country": "dk"
  },
  {
    "position": 16,
    "club": "Lyon",
    "country": "fr"
  },
  {
    "position": 16,
    "club": "Marseille",
    "country": "fr"
  },
  {
    "position": 16,
    "club": "Milan",
    "country": "it"
  },
  {
    "position": 16,
    "club": "Roma",
    "country": "it"
  },
  {
    "position": 16,
    "club": "Valencia",
    "country": "es"
  },
  {
    "position": 32,
    "club": "Ajax",
    "country": "nl"
  },
  {
    "position": 32,
    "club": "Auxerre",
    "country": "fr"
  },
  {
    "position": 32,
    "club": "Basel",
    "country": "ch"
  },
  {
    "position": 32,
    "club": "Benfica",
    "country": "pt"
  },
  {
    "position": 32,
    "club": "Braga",
    "country": "pt"
  },
  {
    "position": 32,
    "club": "Bursaspor",
    "country": "tr"
  },
  {
    "position": 32,
    "club": "CFR Cluj",
    "country": "ro"
  },
  {
    "position": 32,
    "club": "Hapoel Tel Aviv",
    "country": "il"
  },
  {
    "position": 32,
    "club": "MŠK Žilina",
    "country": "sk"
  },
  {
    "position": 32,
    "club": "Panathinaikos",
    "country": "gr"
  },
  {
    "position": 32,
    "club": "Partizan",
    "country": "rs"
  },
  {
    "position": 32,
    "club": "Rangers",
    "country": "sco"
  },
  {
    "position": 32,
    "club": "Rubin Kazan",
    "country": "ru"
  },
  {
    "position": 32,
    "club": "Spartak Moscow",
    "country": "ru"
  },
  {
    "position": 32,
    "club": "Twente",
    "country": "nl"
  },
  {
    "position": 32,
    "club": "Werder Bremen",
    "country": "de"
  }
]